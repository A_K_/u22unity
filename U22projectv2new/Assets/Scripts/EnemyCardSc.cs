﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyCardSc : MonoBehaviour
{
    GameMaster gm;
    int cardID = 0;
    int drawnum = 0;
    int cooldown = 0;
    int carddmg = 0;
    int setnum = 0;
    int multizoomnum = 0;
    bool setmoveflag;
    bool cooltimedflag;
    public bool setzoomflag;
    public bool setzoomreverse;
    public bool cardopenflag;
    public bool cooltimeposmoveflag;
    bool clickzoommoveflag;
    bool zoomedflag;
    float time = 0;
    Vector3 setedpos;
    Vector3 zoombeforpos;
    Vector3 size;
    Vector3 CTpos;
    Vector3 CTsize;
    [SerializeField] MeshCollider mesh;
    [SerializeField] TextMeshPro FlavorText;
    [SerializeField] TextMeshPro CardTitleText;
    [SerializeField] TextMeshPro CoolDowntext;
    [SerializeField] RectTransform CoolDowntextscale;
    [SerializeField] TextMeshPro dmgtext;
    // Start is called before the first frame update
    void Start()
    {
        size = transform.localScale;
        CTpos = CoolDowntextscale.localPosition;
        CTsize = CoolDowntextscale.localScale;
        this.gameObject.SetActive(false);
    }

    void Update()
    {
        if (setmoveflag)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-1.37f + setnum * 0.23f, 0.15f, -3.27f + setnum * 0.27f), 0.05f);
            time += Time.deltaTime;
            if (time > 1f)
            {
                setedpos = transform.position;
                setmoveflag = false;
                time = 0;
            }
        }
        if (cardopenflag)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, -48, 0), 6f);
            time += Time.deltaTime;
            if (time > 1.5f)
            {
                cardopenflag = false;
                time = 0;
            }
        }
        if (setzoomflag) {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-0.148f - multizoomnum * 0.085f, 2.676f + multizoomnum * 0.008f, -5.341f + multizoomnum * 0.1f), 0.4f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(-30.235f, -1.12f, -3.703f), 17f);
        }
        if (setzoomreverse)
        {
            transform.position = Vector3.MoveTowards(transform.position, setedpos, 0.6f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, -48, 0), 17f);
            time += Time.deltaTime;
            if (time > 1f)
            {
                setzoomreverse = false;
                time = 0;
            }
        }
        if (cooltimeposmoveflag)
        {
            if (cardID < 12)
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(-0.013f + cardID * 0.0410f, 2.7251f, -5.0321f), 0.6f);
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(-0.013f + (cardID - 12) * 0.0410f, 2.7f, -5.076f), 0.6f);
            }
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(-30, 0, 0), 15f);
            transform.localScale = Vector3.MoveTowards(transform.localScale, new Vector3(0.004f, 0.004f, 0.0047f), 0.6f);
            time += Time.deltaTime;
            if (time > 1f)
            {
                cooltimedflag = true;
                cooltimeposmoveflag = false;
                time = 0;
            }
        }
        if (clickzoommoveflag)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, size, 0.6f);
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(0f, 2.73f, -5.36f), 3f);//手前に移動して拡大

            time += Time.deltaTime;
            if (time > 1f)
            {
                clickzoommoveflag = false;
                time = 0;
            }
        }
        if (zoomedflag)
        {
            if (Input.GetMouseButtonDown(0))//ズーム解除
            {
                cooltimeposmoveflag = true;
                clickzoommoveflag = false;
                zoomedflag = false;
                time = 0;
                CoolDowntextscale.localPosition = new Vector3(4.7f, 0.22f, -1.23f);
                CoolDowntextscale.localScale = new Vector3(3.05f, 3.05f, 3.05f);
            }
        }
    }

    public void ClickZoom()
    {
        if (zoomedflag== false&& cooltimedflag)
        {
            time = 0;
            zoombeforpos = transform.position;
            clickzoommoveflag = true;
            zoomedflag = true;
            CoolDowntextscale.localPosition = CTpos;
            CoolDowntextscale.localScale = CTsize;
        }
    }

    public void SetMove(int getsetnum, Vector3 pos)
    {
        this.gameObject.SetActive(true);
        transform.localScale = size;
        transform.position = pos;//カード射出位置
        transform.rotation = Quaternion.Euler(0, -48, 180);
        CoolDowntextscale.localPosition = CTpos;
        CoolDowntextscale.localScale = CTsize;
        setnum = getsetnum;
        setmoveflag = true;
    }
    public void SetZoom(int getmulti)
    {
        time = 0;
        multizoomnum = getmulti;
        setzoomflag = true;
        setmoveflag = false;
        setzoomreverse = false;
    }
    public void SetZoomReverse()
    {
        time = 0;
        multizoomnum = 0;
        setzoomflag = false;
        setzoomreverse = true;
    }
    public void CoolTimePosMove()
    {
        time = 0;
        mesh.enabled = true;
        cooltimedflag = true;
        setzoomreverse = false;
        cooltimeposmoveflag = true;
        CoolDowntextscale.localPosition = new Vector3(4.7f, 0.22f, -1.23f);
        CoolDowntextscale.localScale = new Vector3(3.05f, 3.05f, 3.05f);
    }
    public void SetCoolTime(int ct)
    {
        cooldown = ct;
        CoolDowntext.text = ct.ToString();
    }

    public void CardPosReset()
    {
        cooltimeposmoveflag = false;
        if (cooldown <= 0)
        {
            mesh.enabled = false;
            cooltimedflag = false;
            this.gameObject.SetActive(false);
            transform.localScale = size;
        }
    }
    public void DecreaseCooldown()
    {
        if (cooldown > 0)
        {
            cooldown--;
            CoolDowntext.text = cooldown.ToString();
        }
    }


    public void SetStatus(GameMaster getgm, int getcardID, int getcooldown, int getdmg, string cardtitle, string flavortext, string getMeasuresText)
    {
        gm = getgm;
        cardID = getcardID;
        cooldown = getcooldown;
        CoolDowntext.text = cooldown.ToString();
        carddmg = getdmg;
        if (cardID > 11)
        {
            dmgtext.text = "";
        }
        else
        {
            dmgtext.text = getdmg.ToString();
        }
        CardTitleText.text = cardtitle;
        FlavorText.text = flavortext;
    }
}
