﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Moviesc : MonoBehaviour
{

    public VideoPlayer mPlayer;
    public RawImage raw;
    bool startflag;
    float alpha = 1f;
    float time;
    

    // Use this for initialization
    void Start()
    {
        raw.color = new Color(1f, 1f, 1f, 1f);
        mPlayer.time = 5f;　//こんな感じで再生途中再生したい時間を
        mPlayer.Play();
        startflag = true;

    }
    void Update()
    {
        if (startflag) { 
            time += Time.deltaTime;
            if (time > 2f&& alpha>0f) {
                alpha -= 0.01f;
                raw.color = new Color(1f, 1f, 1f, alpha);
            }
            if (time>6f)
            {
                time = 0;
                enabled = false;
                startflag = false;
            }
        }
    }
}
