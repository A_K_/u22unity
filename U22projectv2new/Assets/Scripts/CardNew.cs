﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CardNew : MonoBehaviour
{
    int cardID = 0;
    int drawnum = 0;
    int cooldown = 0;
    int carddmg = 0;
    int setnum = 0;
    int multizoomnum = 0;
    float nowtime;
    bool setmoveflag;
    bool drawmoveflag;
    bool zoommoveflag;
    bool setzoomreverseflag;
    bool cooltimeposmoveflag;
    public bool cardopenflg;
    public bool battlezoommoveflag;
    bool dragflag;
    bool cardtype_def = false;
    public bool seted = false;
    bool defposIDcheckflg;
    bool battlezoombackflg;
    public bool Pointenterflg = false;
    Vector3 drawpos;
    Vector3 setpos;
    Quaternion setQuaternion;
    [SerializeField] MeshCollider mesh;
    [SerializeField] GameMaster gm;
    [SerializeField] TextMeshPro FlavorText;
    [SerializeField] TextMeshPro CardTitleText;
    [SerializeField] TextMeshPro CoolDowntext;
    [SerializeField] TextMeshPro dmgtext;
    [SerializeField] TextMeshPro MeasuresText;
    [SerializeField] GameObject MeasuresPlane;
    [SerializeField] ParticleSystem selectparticle;
    [SerializeField] AudioSource[] Audio;

    void Start()
    {

    }

    void Update()
    {
        if (cardtype_def && drawmoveflag)//防御カードの手札移動処理
        {
            /////////////////////////////////////////////////最初の位置//////////////////目標地点X//////////Y//////Z////到達時間（速度）////
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-1.3f + drawnum * 0.3f, 1.069f, -5.19f), 1f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(-15f, 0, 0f), 10f);
            if (transform.position.x == -1.3f + drawnum * 0.3f)//目標地点に到達したら
            {
                drawpos = transform.position;
                mesh.enabled = true;
                drawmoveflag = false;
                dragflag = false;
            }
        }
        if (cardtype_def == false && drawmoveflag)//攻撃カードの手札移動処理
        {

            /////////////////////////////////////////////////最初の位置//////////////////目標地点X//////////Y//////Z////到達時間（速度）////
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-1.86f + drawnum * 0.3f, 0.225f, -5.03f), 1f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(-15f, 0, 0f), 10f);
            /*扇状テスト
             * if (drawnum < 6&& MainGame.mydefcardcount>6)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(30, 150f + 5f*drawnum, 0), 3f);

            }
            if (drawnum > 6 && MainGame.mydefcardcount > 6)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(30, 150f + 5f * drawnum, 0), 3f);

            }
            */

            nowtime += Time.deltaTime;
            if (nowtime >= 0.5f)//一定時間経過したら
            {
                drawpos = transform.position;
                mesh.enabled = true;
                drawmoveflag = false;
                dragflag = false;
                nowtime = 0;
            }
        }
        if (zoommoveflag == true)//カード拡大移動処理
        {
            if (Input.GetMouseButtonDown(0))//ズーム解除
            {
                zoommoveflag = false;
                drawmoveflag = true;
                MeasuresPlane.SetActive(false);//対処テキストを非表示
                if (defposIDcheckflg)
                {
                    for (int i = 0; i < 20; i++)
                    {
                        if (gm.defcard_table2[cardID, i] == true && gm.setdefpos[i] == false)
                        {
                            gm.deficonobjimage[i].color = new Color(255, 255, 255, 0);
                        }
                    }
                }
                gm.CardSelectParticleCheck();
            }
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(0f, 2.73f, -5.36f), 3f);//手前に移動して拡大
            /*
            nowtime += Time.deltaTime;
            if (nowtime >= 2f)//一定時間経過したら
            {
                zoommoveflag = false;
                nowtime = 0;
            }
            */

        }
        if (setmoveflag == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(0.4f + 0.25f * setnum, 0.16f, -4.4f + 0.2f * setnum), 0.2f);//カードセット
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(180, -35.91f, 0f), 6f);
            nowtime += Time.deltaTime;
            if (nowtime > 1f)
            {
                nowtime = 0;
                setpos = transform.position;
                setmoveflag = false;
            }
        }
        if (battlezoommoveflag == true)//戦闘演出時の拡大移動処理
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(0.18f + multizoomnum * 0.086f, 2.699f + multizoomnum * 0.031f, -5.361f + multizoomnum * 0.056f), 0.4f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(-27.473f, 6.668f, 7.013f), 17f);
            nowtime += Time.deltaTime;
            if (nowtime > 1f)
            {
                battlezoommoveflag = false;
                nowtime = 0;
            }
        }

        if (setzoomreverseflag == true)//戦闘演出時のカードを元の位置に移動する処理
        {
           transform.position = Vector3.MoveTowards(transform.position, setpos, 0.6f);
           transform.rotation = Quaternion.RotateTowards(transform.rotation, setQuaternion, 15f);
            nowtime += Time.deltaTime;
            if (nowtime > 1f)
            {
                setzoomreverseflag = false;
                nowtime = 0;
                multizoomnum = 0;
            }

        }
        if (cardopenflg)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(-188, -48, -180), 6f);
            nowtime += Time.deltaTime;
            if (nowtime > 1f)
            {
                cardopenflg = false;
                setQuaternion = transform.rotation;
                nowtime = 0;
            }
        }
    }
    public void SetZoom(int getmultizoomnum)
    {
        multizoomnum = getmultizoomnum;
        nowtime = 0;
        cardopenflg = false;
        battlezoommoveflag = true;
        setzoomreverseflag = false;
    }


    public void SetZoomReverse()
    {
        nowtime = 0;
        multizoomnum = 0;
        battlezoommoveflag = false;
        setzoomreverseflag = true;
    }

    public void OnDrag()
    {
        if (defposIDcheckflg == false)//防げるポジションチェックフラグ
        {
            for (int i = 0; i < 20; i++)
            {
                if (gm.defcard_table2[cardID, i] == true && gm.setdefpos[i] == false)
                {
                    gm.deficonobjimage[i].color = new Color(255, 255, 255, 0.3f);
                }
            }
            defposIDcheckflg = true;
        }
        if (cooldown <= 0 && seted == false)
        {
            if (zoommoveflag) transform.position = drawpos;
            zoommoveflag = false;
            dragflag = true;
            Vector3 TapPos = Input.mousePosition;
            TapPos.z = 3f;
            transform.position = Camera.main.ScreenToWorldPoint(TapPos);
            if (TapPos.y < 120)
            {
                transform.position = drawpos;
            }
        }
    }
    public void ClickZoom()
    {
        if (dragflag == false)
        {
            MeasuresPlane.SetActive(true);//対処テキストを表示
            zoommoveflag = true;
            if (defposIDcheckflg == false)//防げるポジションチェックフラグ
            {
                for (int i = 0; i < 20; i++)
                {
                    if (gm.defcard_table2[cardID, i] == true && gm.setdefpos[i] == false)
                    {
                        gm.deficonobjimage[i].color = new Color(255, 255, 255, 0.3f);
                    }
                }
                defposIDcheckflg = true;
            }
            selectparticle.Stop();
        }
    }

    //カードセット処理
    public void MouseRelease()
    {
        if (seted == false && dragflag == true)
        {
            setnum = gm.GetSetCardCount(1);
            Vector3 TapPos = Input.mousePosition;
            TapPos.z = 3f;
            transform.position = Camera.main.ScreenToWorldPoint(TapPos);
            if (setnum < 3 && cooldown <= 0 && TapPos.y >= 85)
            {
                setmoveflag = true;
                for (int i = 0; i < 20; i++)
                {
                    if (gm.defcard_table2[cardID, i] == true && gm.setdefpos[i] == false)
                    {
                        gm.setdefpos[i] = true;//防御位置に対応している場所のアイコンを表示
                        gm.deficonobjimage[i].color = new Color(255, 255, 255, 255);//透明度で表示を切り替え
                    }
                }
                defposIDcheckflg = false;
                gm.SetCardid(gm.GetSetCardCount(1), cardID);//使用IDセット 
                Debug.Log("セットカードID=" + cardID);
                //this.name = "SetCard+" + cardID;
                gm.SetCardCount(1, ++setnum);
                //this.tag = "SetCard";
                Audio[0].Play();
                seted = true;
                selectparticle.Stop();
            }
            else
            {
                drawmoveflag = true;
            }
        }
        if (defposIDcheckflg)
        {
            for (int i = 0; i < 20; i++)
            {
                if (gm.defcard_table2[cardID, i] == true && gm.setdefpos[i] == false)
                {
                    gm.deficonobjimage[i].color = new Color(255, 255, 255, 0);
                }
            }
        }
        defposIDcheckflg = false;
    }

    public void Draw(int getdrawnum)
    {
        if (!seted)//セットされていなかったら
        {
            enabled = true;
            transform.position = new Vector3(1.233f, 1.21f, -5.183f);//引き始めの初期位置
            Audio[0].Play();
            drawnum = getdrawnum;
            drawmoveflag = true;
        }
    }

    public void SetStatus(GameMaster getgm,int getcardID, int getcooldown, int getdmg, string cardtitle, string flavortext, string getMeasuresText)
    {
        gm = getgm;
        cardID = getcardID;
        cooldown = getcooldown;
        CoolDowntext.text = cooldown.ToString();
        carddmg = getdmg;
        if (cardID > 11)
        {
            dmgtext.text = "";
            cardtype_def = true;
            drawnum = getcardID - 12;
        }
        else
        {
            dmgtext.text = getdmg.ToString();
            drawnum = getcardID;
        }
        CardTitleText.text = cardtitle;
        FlavorText.text = flavortext;
        MeasuresText.text = getMeasuresText;
    }

    public void SetCooldown(int cd)
    {
        cooldown = cd;
        CoolDowntext.text = cooldown.ToString();
    }

    public void DecreaseCooldown()
    {
        if(cooldown>0) cooldown--;
        CoolDowntext.text = cooldown.ToString();
    }

    public void SelectParticlePlay()
    {
        if (cooldown <= 0 && seted == false && gm.GetSetCardCount(1) < 3)
        {
            selectparticle.Play();
        }
        else selectparticle.Stop();
    }
    public void SelectParticleStop()
    {
        selectparticle.Stop();
    }
    public void CardPosReset()
    {
        if (!seted)//セットされていなかったら
        {
            this.gameObject.SetActive(false);
            transform.rotation=Quaternion.Euler(-15f, 0, 0f);
        }
    }

}
