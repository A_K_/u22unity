﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerClass
{ 
    public int userid = 0;
    public int roomid = 0;
    public int playerid = 0;
    public int hp = 100;
    public int cardsetcount = 0;
    public int[] setcardid = {-1,-1,-1};
    public bool turnEndflag;
    public int[] cooldown= new int[20];

}
