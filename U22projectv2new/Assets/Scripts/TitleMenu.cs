﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.Networking;

public class TitleMenu : MonoBehaviour
{
    int time;
    public HTTPscript http;
    public Text networktext;
    public Text inputtext;
    public GameObject loadingcard;
    
    public void StartOnClick()
    {
        if (!networktext.text.StartsWith("接続中")) {
                http.errortext = "";
                http.Sendname(inputtext.text);//インプットネームを送信
                networktext.text = "接続中";
                StartCoroutine(GetRoomStatus());
        }
    }
    void Update()
    {
        loadingcard.transform.Rotate(-1.7f, 0f, 2f);

        ///////デバッグ操作
        if (Input.GetKeyDown(KeyCode.F1))
        {
            http.roomID = "111";
            http.userNumber = "1";
            http.userID = "1";
            http.response_str = "-1,1,1";
            http.cardtable = "1,1,1,s,1,1,1,s,1,1,1,s,1,1,1,s,1,1,1,s,1,1,1,s,1,1,1";
            http.taioulist = "0,13,14,-1,-1,-1,s,1,17,19,-1,-1,-1,s,2,13,18,-1,-1,-1,s,3,12,15,-1,-1,-1,s,4,17,19,-1,-1,-1,s,5,12,18,-1,-1,-1,s,6,12,15,-1,-1,-1,s,7,14,19,-1,-1,-1,s,8,12,18,-1,-1,-1,s,9,16,18,-1,-1,-1,s,10,17,19,-1,-1,-1,s,11,12,16,-1,-1,-1,s,12,3,5,6,8,11,s,13,0,2,-1,-1,-1,s,14,0,7,-1,-1,-1,s,15,3,6,-1,-1,-1,s,16,9,11,-1,-1,-1,s,17,1,4,10,-1,-1,s,18,5,8,9,-1,-1,s,19,1,2,4,7,10";
        }
        if (Input.GetKeyDown(KeyCode.F2))//接続リセット
        {
            networktext.text = "リセット";
            StopAllCoroutines();
        }
    }

    IEnumerator GetRoomStatus()
    {
        while (http.roomID=="-1")//ルームIDが変わるまで取得
        {
            networktext.text = "接続中" + time + "秒経過\n"+http.errortext+"\n"+ http.debugstr;
            time++;
            yield return new WaitForSeconds(1f);
            if (time > 5 && http.roomID.Equals("-1"))//5秒待っても繋がらなければ中断
            {
                networktext.text = "接続エラー\n"+http.errortext + "\n" + http.debugstr;
                time = 0;
                yield break;
            }
        }
        //ルームIDを取得後
        time = 0;
        http.Gettext("room.txt");
        http.GetCardTable();
        while (!http.response_str.StartsWith("-1,1,1"))//ルームの両方の入室確認
        {
            networktext.text = "接続中" + time+"秒経過\nROOM"+http.roomID+ " userNumber=" + http.userNumber+" userID=" +http.userID+" 入室完了\nプレイヤー2待機中...\n" + http.response_str +"\n"+ http.debugstr;
            time ++;
            yield return new WaitForSeconds(1f);
            if (time >60 && http.response_str.StartsWith("接続エラー"))//60秒待ってもエラーなら中断
            {
                networktext.text = "接続エラー\n" + http.errortext + "\n" + http.debugstr;
                time = 0;
                yield break;
            }
            if(time%3==0) http.Gettext("room.txt");//3秒毎に再取得
        }
        //通信成功したら
        /*
        ////プレイヤーネーム取得処理
        http.Gettext("name.txt");
        yield return new WaitForSeconds(1f);
        while (http.roomID != "")//中身をを取得するまでループ
        {
            networktext.text = "接続中" + time + "秒経過\nROOM" + http.roomID + " userNumber=" + http.userNumber + " userID=" + http.userID + " 入室完了\nプレイヤー名取得中\n" + http.response_str + "\n" + http.debugstr;
            time++;
            yield return new WaitForSeconds(1f);
            if (time > 70 && http.response_str.StartsWith("接続エラー"))//全体で70秒待ってもエラーなら中断
            {
                networktext.text = "接続エラー\n" + http.errortext + "\n" + http.debugstr;
                time = 0;
                yield break;
            }
            if (time % 2 == 0) http.Gettext("name.txt");//2秒毎に再取得
        }
        http.namestr = http.response_str.Split(',');//name.txtをカンマでスプリットして格納
        */
        networktext.text = "接続中" + time+"\n開始";
        SceneManager.LoadScene("GameScene");
    }

    void printlog()
    {

    }

}
