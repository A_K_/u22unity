﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 流れる上下のテキスト用クラス
/// </summary>
public class ScrollText : MonoBehaviour
{
    public bool isright;//右側用のスクロールか
    RectTransform rect;
    int moveframe;

    void Start()
    {
        rect = GetComponent<RectTransform>();
    }
    void Update()
    {
        if(isright) rect.localPosition += new Vector3(7, 0, 0);//-740から-425まで移動
        else rect.localPosition -= new Vector3(7, 0, 0);
        moveframe++;
        if (moveframe>44)
        {
            if (isright) rect.localPosition -= new Vector3(315, 0, 0);
            else rect.localPosition += new Vector3(315, 0, 0);
            moveframe = 0;
        }
    }
}