﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// テキストファイルを分割して数値や配列で返すクラス
/// </summary>
public class TextReadSplitter
{
    public int TextReadSplit(int row_number, int col_number, string getstr)//参照したい行と列とカンマ区切りのstrを受け取って、中身の値で返すお
    {
        int[,] array = CountArray(getstr);//テキストの行と列数を数える
        //ここから
        /*
        string[] temparray = getstr.Split(',');
        for (int i = 0, j = 0, k = 0; k < temparray.Length; k++)
        {
            if (!temparray[k].Equals("s"))
            {
                array[i, j] = int.Parse(temparray[k]);
                j++;
            }
            else
            {
                i++;
                j = 0;
            }
        }
        */
        //ここまで古いやつ

        //ここから新しいやつ
        string[] temparray = getstr.Split('s');
        for(int i = 0;i<array.GetLength(0);i++)
        {
            string[] temparray2 = temparray[i].Split(',');
            for(int j = 0;j<array.GetLength(1);i++)
            {
                array[i, j] = int.Parse(temparray2[j]);
            }
        }
        //

        return array[row_number, col_number];
    }

    public int[] TextReadSplit_array(int line_number, int[,] alldata, string getstr)//列で返すお
    {
        //ここから
        /*
        string[] array = getstr.Split(',');
        for (int i = 0, j = 0, k = 0; k < array.Length; k++)
        {
            if (!array[k].Equals("s"))
            {
                alldata[i, j] = int.Parse(array[k]);
                j++;
            }
            else
            {
                i++;
                j = 0;
            }
        }
        */
        //ここまで古いやつ

        //ここから新しいやつ
        string[] array = getstr.Split('s');
        for(int i = 0;i<alldata.GetLength(0);i++)
        {
            string[] array2 = array[i].Split(',');
            for(int j = 0;j<alldata.GetLength(1);j++)
            {
                alldata[i, j] = int.Parse(array2[j]);
            }
        }
        //

        int[] data = new int[alldata.GetLength(1)];
        for (int i = 0; i < data.Length; i++)
        {
            data[i] = alldata[line_number, i];
        }

        return data;
    }
    int[,] CountArray(string getstr)//テキストの行数と列数を数える
    {
        int count_row = 0;
        int count_col = 0;
        /*ここから
        string[] temparray = getstr.Split(',');
        for (int j = 0, k = 0; k < temparray.Length; k++)//iは縦軸移動、jは横軸移動、kは文字数
        {
            if (temparray[k] != "s")//sは改行用文字、"s"ではなかったら右に一文字移動
            {
                j++;
            }
            else//"s"だったら改行する
            {
                count_row++;
                count_col = j;
                j = 0;
            }
        }
        count_row++;//ループを抜けた後、行数が1個足りないので足す
        int[,] array = new int[count_row, count_col];
        //ここまで古いやつ
        */
        //ここから新しいやつ
        string[] temparray = getstr.Split('s');
        count_row = temparray.Length;

        string[] temparray2 = temparray[0].Split(',');
        count_col = temparray2.Length;

        int[,] array = new int[count_row, count_col];
        //

        return array;
    }
}