﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class GameEndSc : MonoBehaviour
{
    public static int endmovie = 0;
    [SerializeField] VideoClip[] video;
    [SerializeField] VideoPlayer mPlayer;

    // Start is called before the first frame update
    void Start()
    {
        mPlayer.clip = video[endmovie];
        mPlayer.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
