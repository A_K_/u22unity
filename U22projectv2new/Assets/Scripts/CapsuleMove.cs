﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMove : MonoBehaviour
{
    Rigidbody rb;
    Renderer rend;
    GameObject Explosion;
    public int player=0;
    float nowtime;
    float bigtime;
    bool colorturn;
    bool damagePopUpflag;
    bool endflag;
    float r = 1;
    float g = 1;
    float b = 1;
    float alpha = 1;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //プレイヤー移動
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        rb.AddForce(x, 0, z);

        if (player == 1) { 
            if (colorturn == false)
            {
                r -= 0.01f;
            }
            else
            {
                r += 0.01f;
            }
            if (r <= 0f) colorturn = true;
            if (r > 1f) colorturn = false;
        }
        else
        {
            if (colorturn == false)
            {
                g -= 0.01f;
                b -= 0.01f;
            }
            else
            {
                g += 0.01f;
                b += 0.01f;
            }
            if (g <= 0f) colorturn = true;
            if (g > 1f) colorturn = false;
        }
        rend.material.color = new Color(r, g, b);

        nowtime += Time.deltaTime;
        if (nowtime >= Random.Range(1f, 3f))//一定時間経過したら
        {
            rb.AddForce(new Vector3(0, 80f, 0));
            nowtime = 0;
        }
        if (endflag)
        {
            transform.localScale= new Vector3(transform.localScale.x+0.01f, transform.localScale.y + 0.01f, transform.localScale.z + 0.01f);
            bigtime += Time.deltaTime;
            if (bigtime >= 3)//一定時間経過したら
            {
                this.
                bigtime = 0;
                alpha -= 0.01f;
                rend.material.color = new Color(r, g, b, alpha);
            }

        }
        if (damagePopUpflag)
        {

        }
    }
    void GameEndExplosion()//終了時爆発演出用
    {
        endflag = true;
    }
}
