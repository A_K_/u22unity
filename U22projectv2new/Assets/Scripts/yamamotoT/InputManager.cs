﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class InputManager : MonoBehaviour
{
    [SerializeField] InputField inputField;

    /// <summary>
    /// Startメソッド
    /// InputFieldコンポーネントの取得および初期化メソッドの実行
    /// </summary>
    void Start()
    {
        InitInputField();
    }



    /// <summary>
    /// Log出力用メソッド
    /// 入力値を取得してLogに出力し、初期化
    /// </summary>

    //ENTERかフォーカスが外れた場合
    public void InputLogger()
    {

        string inputValue = inputField.text;
        StartCoroutine(GetData("http://localhost:8080/UnityTest/access", inputValue));

        Debug.Log(inputValue);
    }

    private IEnumerator GetData(string url,string inputname)
    {
        //formのparameter生成
        WWWForm form = new WWWForm();
        form.AddField("name", inputname);

        //server呼び出し
        UnityWebRequest www = UnityWebRequest.Post(url, form);
        yield return www.SendWebRequest();

        //接続エラー
        if (www.isNetworkError || www.isHttpError)
        {
            //error
            Debug.Log(www.error);
        }
        //接続成功
        else
        {
            Debug.Log("Form upload complete!");

            //結果をJSONで受け取る
            var jsonDict = MiniJSON.Json.Deserialize(www.downloadHandler.text) as Dictionary<string, object>;
            Debug.Log(jsonDict["param"]);
        }
    }


    /// <summary>
    /// InputFieldの初期化用メソッド
    /// 入力値をリセットして、フィールドにフォーカスする
    /// </summary>


    void InitInputField()
    {

        // 値をリセット
        inputField.text = "";

        // フォーカス
        inputField.ActivateInputField();
    }

}
