﻿using System.Net;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class OnRequestEvent : UnityEvent<HttpListenerContext> { }

public class HttpServer : MonoBehaviour
{
    private HttpListener httpListener = new HttpListener();

    // エディター上から設定する
    public int port = 8080;
    public string path = "/";
    public bool startOnAwake = true;

    // リクエスト処理するイベントハンドラー。エディター上から設定する。
    public OnRequestEvent OnRequest;

    void Start()
    {
        httpListener.Prefixes.Add("http://*:" + port + path); // http://*:8080/ のようになる

        // コンポーネントの開始時に自動起動
        if (startOnAwake)
        {
            StartServer();
        }
    }

    // リクエストの受け付けを開始する
    public async Task StartServer()
    {
        httpListener.Start();

        while (true)
        {
            // リモートからの接続を待機
            var context = await httpListener.GetContextAsync();

            // 着信ログを表示
            Debug.Log("Request path: " + context.Request.RawUrl);

            OnRequest.Invoke(context);
        }
    }

    // サーバーを停止する
    public void StopServer()
    {
        httpListener.Stop();
    }

    // 破棄時にサーバーを止める
    void OnDestroy()
    {
        StopServer();
    }
}