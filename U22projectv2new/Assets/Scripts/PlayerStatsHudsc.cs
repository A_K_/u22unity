﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsHudsc : MonoBehaviour {

    private RectTransform rectComponent;
    private Image imageComp;

    public float speed = 200f;
    public Text text;
    public Text manatext;


    // Use this for initialization
    void Start () {
        rectComponent = GetComponent<RectTransform>();
        imageComp = rectComponent.GetComponent<Image>();
        imageComp.fillAmount = 1.0f;
    }

    // Update is called once per frame
    void Update () {
        /*　毎フレーム減少処理
        int a = 100;
        if (imageComp.fillAmount != 0f)
        {
            imageComp.fillAmount -= Time.deltaTime * speed;
            a = (int)(imageComp.fillAmount * 100);
            text.text = a + "%";
        }
        else
        {
            imageComp.fillAmount = 1.0f;
            text.text = "100%";
        }
        */
        imageComp.transform.Rotate(0.0f, 0.0f, 1.0f);

    }
}
