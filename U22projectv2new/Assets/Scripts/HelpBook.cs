﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpBook : MonoBehaviour
{
    [SerializeField] AudioSource audio;
    [SerializeField] RawImage howtoimage;
    [SerializeField] Texture[] tex;
    [SerializeField] GameObject buttonleft, buttonright=default;
    private bool isopen;
    private int page;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void onclick()
    {
        audio.Play();
        if (!isopen)
        {
            howtoimage.texture = tex[0];
            howtoimage.enabled = true;
            buttonleft.SetActive(true);
            buttonright.SetActive(true);
            isopen = true;
        }
        else
        {
            howtoimage.enabled = false;
            buttonleft.SetActive(false);
            buttonright.SetActive(false);
            isopen = false;
            page = 0;
        }
    }

    public void pageright()
    {
        audio.Play();
        if (page < 5)page++;
        howtoimage.texture = tex[page];
    }
    public void pageleftt()
    {
        audio.Play();

        if (page > 0)page--;
        howtoimage.texture = tex[page];
    }
}
