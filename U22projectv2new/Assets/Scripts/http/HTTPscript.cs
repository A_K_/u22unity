﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HTTPscript : MonoBehaviour
{
    public string roomID;
    public string userNumber;
    public string userID;
    public string errortext;
    public string[] usehand = {"-1","-1","-1"};
    public string response_str;//通信で受け取ったテキストを格納
    public string cardtable;//ID,ダメージ,コストの一覧txtを受け取る
    public string taioulist;//IDに対応しているIDの一覧txtを受け取る
    public string[] namestr={"-1","P1","P2"};//playername.txtを,でスプリットした文字を格納
    public string debugstr="";
    public bool connecting = false;//true=接続中（再取得不可状態）、false=接続していない（通信OK）
    public bool uploading = false;//同上
    private string yturl = "http://153.122.72.200";
    private string posturl;
    TextReadSplitter txtsplit = new TextReadSplitter();

    private void Start()
    {
        Application.targetFrameRate = 60;//60fps制限
        roomID = "-1";
        userNumber = "-1";
        userID = "-1";
        errortext = "";
        response_str = "";
        posturl = yturl + ":8080/git1/waithi1";
        DontDestroyOnLoad(this);//シーンが変わってもオブジェクトを残す処理
    }

    public void Sendname(string input_name)//初回のユーザーネームを送信するコルーチンを実行
    {
        errortext = "";
        if (!connecting) StartCoroutine(StartUpload(input_name));
    }

    public void Gettext(string textpass)//room.txtとユーザー.txtの情報を受信するコルーチンを実行
    {
        response_str = "";
        if (!connecting)StartCoroutine(WebRequesGet(textpass));//webRequest通信中でなければ通信して取得
    }
    /*
    public int Gettext(string textpass,int getrow,int getcol)//指定されたテキストのX行目とY列目を参照して中身をintで返す
    {
        response_str = "";
        StartCoroutine(WebRequesGet(textpass));

        return txtsplit.TextReadSplit()
    }*/

    public void GetCardTable()//初回にカード情報を受信するコルーチンを実行
    {
        StartCoroutine(WebRequesGetCardTable());
    }

    public void Usecardupload(int[] usecardid)//使用手札を送信するコルーチンを実行
    {
        uploading = true;
        for (int i = 0; i < 3; i++)
        {
            usehand[i]=usecardid[i].ToString();
            Debug.Log(usehand[i]);
        }
        StartCoroutine(Upload());
    }

    IEnumerator StartUpload(string input_name)//初回の通信
    {
        connecting = true;
        WWWForm formData = new WWWForm();
        formData.AddField("name", input_name);

        UnityWebRequest www = UnityWebRequest.Post(posturl, formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            debugstr = www.error;
            connecting = false;
            yield break;
        }
        else//通信成功
        {
            Debug.Log("Form upload complete!");
            var jsonDict = MiniJSON.Json.Deserialize(www.downloadHandler.text) as Dictionary<string, object>;
                Debug.Log((string)(jsonDict["param"]));
                JsonDataClass jsonDataClass;
                jsonDataClass = JsonUtility.FromJson<JsonDataClass>((string)(jsonDict["param"]));
                roomID = jsonDataClass.roomID;
                userNumber = jsonDataClass.userNumber;
                userID = jsonDataClass.userID;
                errortext = jsonDataClass.error;
            connecting = false;
            Debug.Log("roomID= " + roomID);
            Debug.Log("userNumber= " + userNumber);
            Debug.Log("userID= " + userID);
            Debug.Log("error=" + errortext);
            /*
            roomID = ;
            userNumber = ;
            userID = ;
            */
            yield break;
        }
    }

    IEnumerator Upload()//2回目以降の通信
    {
        WWWForm formData = new WWWForm();
        formData.AddField("roomID", roomID);
        formData.AddField("userNumber", userNumber);
        formData.AddField("userID", userID);
        formData.AddField("usehand1", usehand[0]);
        formData.AddField("usehand2", usehand[1]);
        formData.AddField("usehand3", usehand[2]);

        UnityWebRequest www = UnityWebRequest.Post(posturl, formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError||www.isHttpError)
        {
            Debug.Log(www.error);
            debugstr = www.error;
            uploading = false;
            yield break;
        }
        else
        {
            Debug.Log("uploadDone!");
            uploading = false;
            yield break;
        }
    }

    IEnumerator ExitUpload()//退出の通信
    {
        WWWForm formData = new WWWForm();
        formData.AddField("roomID", roomID);
        formData.AddField("userNumber", userNumber);
        formData.AddField("userID", userID);
        formData.AddField("flag", "2");

        UnityWebRequest www = UnityWebRequest.Post(posturl, formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            yield break;
        }
        else
        {
            Debug.Log("Form upload complete!");
            yield break;
        }
    }

    IEnumerator WebRequesGet(string textpass)//テキスト情報を取得するコルーチン
    {
        connecting = true;//通信中フラグを立てる
        UnityWebRequest www = UnityWebRequest.Get(yturl +"/"+ roomID +"/"+ textpass);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            debugstr = www.error;
            connecting = false;//通信中フラグを下げる
            yield break;
        }
        else // ダウンロードが正常に完了した
        {
            response_str = www.downloadHandler.text;
            Debug.Log("DL完了="+response_str);
            connecting = false;//通信中フラグを下げる
            yield break;
        }
    }

    IEnumerator WebRequesGetCardTable()//対応表とカードコストとユーザーネーム取得用
    {
        UnityWebRequest www = UnityWebRequest.Get(yturl + "/" + roomID + "/card.txt");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Debug.Log("card.txt取得エラー");
            
        }
        else // ダウンロードが正常に完了した
        {
            Debug.Log("card.txtダウンロードが正常に完了した");
            cardtable = www.downloadHandler.text;
            Debug.Log(cardtable);
        }

        UnityWebRequest www2 = UnityWebRequest.Get(yturl + "/" + roomID + "/taiou.txt");
        yield return www2.SendWebRequest();

        if (www2.isNetworkError || www2.isHttpError)
        {
            Debug.Log(www2.error);
            Debug.Log("taiou.txt取得エラー");
            yield break;

        }
        else // ダウンロードが正常に完了した
        {
            Debug.Log("taiou.txtダウンロードが正常に完了した");
            taioulist = www2.downloadHandler.text;
            Debug.Log(taioulist);
            yield break;

        }

        /*
        www = UnityWebRequest.Get(yturl + "/" + roomID + "/name.txt");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Debug.Log("name.txt取得エラー");
            yield break;
        }
        else // ダウンロードが正常に完了した
        {
            Debug.Log("taiou.txtダウンロードが正常に完了した");
            nametxt = www.downloadHandler.text;
            Debug.Log(nametxt);
            yield break;
        }
        */
    }

    /*
    IEnumerator wwwGet(string textpass)//wwwの場合(WebRequesGetの旧ver) 未使用
    {

        //WWW www = new WWW("http://192.168.11.61/" + textpass);
        WWW www = new WWW("http://192.168.11.61/room1/texttest.txt");//テスト用ファイル
    

        while (!www.isDone)
        { // ダウンロードの進捗を表示
            response_str = "取得中";
            yield return null;
        }

        if (!string.IsNullOrEmpty(www.error))
        { // ダウンロードでエラーが発生した
            response_str = "通信エラー";
            Debug.Log(www.error);
        }
        else
        { // ダウンロードが正常に完了した
            Debug.Log("ダウンロードが正常に完了した");
            response_str = www.text;
        }
    }
    */
}
