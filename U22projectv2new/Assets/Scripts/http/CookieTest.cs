﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CookieTest : MonoBehaviour {


    public string stDisplay;
    //public Hashtable hsHeader;

    // Use this for initialization
    void Start () {
      //          this.hsHeader = new Hashtable();
               this.stDisplay = "none";
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(8, 8, Screen.width - 32, 32), "Click"))
        {
            StartCoroutine(GetData("http://localhost:8080/UnityTest/access"));
        }
        GUILayout.BeginArea(new Rect(8, 40, Screen.width - 32, Screen.height - 16 - 32));
        GUILayout.Label(this.stDisplay);
        GUILayout.EndArea();
    }

    private IEnumerator GetData(string url)
    {
        //formのparameter生成
        WWWForm form = new WWWForm();
        form.AddField("userID", "yamamoto");
        form.AddField("roomid","001");

        //server呼び出し
        UnityWebRequest www = UnityWebRequest.Post(url, form);
        yield return www.SendWebRequest();

        //接続エラー
        if (www.isNetworkError || www.isHttpError)
        {
            //error
            Debug.Log(www.error);
        }
        //接続成功
        else
        {
            Debug.Log("Form upload complete!");

            //結果をJSONで受け取る
            var jsonDict = MiniJSON.Json.Deserialize(www.downloadHandler.text) as Dictionary<string, object>;
            Debug.Log(jsonDict["param"]);
            this.stDisplay = (string)(jsonDict["param"]);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
