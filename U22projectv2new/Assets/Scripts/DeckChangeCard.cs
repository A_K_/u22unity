﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckChangeCard : MonoBehaviour
{
    [SerializeField] GameMaster gm;
    [SerializeField] Texture[] cardimage;
    Renderer rend;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }
    void Update()
    {
        transform.Rotate(-1.7f, 0f, 2f);
    }
    public void Clickevent()
    {
        if (gm.deckchangeflg)
        {
            gm.CardPosResetOrder();
            if (gm.decktypeisDEF == false)
            {
                rend.material.mainTexture = cardimage[1];//カードの画像を切替
                gm.decktypeisDEF = true;
                gm.DrawDef();
            }
            else
            {
                rend.material.mainTexture = cardimage[0];//カードの画像を切替
                gm.decktypeisDEF = false;
                gm.DrawATK();
            }
        }
    }
}
