﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTEST : MonoBehaviour
{
    public ParticleSystem m_particleSystem;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            m_particleSystem.Play();
        }
    }
}