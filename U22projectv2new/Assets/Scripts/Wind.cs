﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 物理演算のオブジェクテスト用
/// </summary>
public class Wind : MonoBehaviour
{
    public float coefficient;   // 空気抵抗係数
    public Vector3 velocity=new Vector3(10f,10f,10f);    // 風速

    void OnTriggerStay(Collider col)
    {
        if (col.GetComponent<Rigidbody>() == null)
        {
            return;
        }

        // 相対速度計算
        var relativeVelocity = velocity - col.GetComponent<Rigidbody>().velocity;

        // 空気抵抗を与える
        col.GetComponent<Rigidbody>().AddForce(coefficient * relativeVelocity);
    }
    public void OnDrag()
    {
        Vector3 TapPos = Input.mousePosition;
        TapPos.z = 4f;
        transform.position = Camera.main.ScreenToWorldPoint(TapPos);
    }
}