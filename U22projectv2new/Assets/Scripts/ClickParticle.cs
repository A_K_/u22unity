﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickParticle : MonoBehaviour
{
    public GameObject particlePrefab; //パーティクルのプレハブ
    [SerializeField] GameObject DRAG_PARTICLE; // PS_DragStarを割り当てること
    [SerializeField] GameObject Canvas; // 生成位置をUIにするため

    // Update is called once per frame
    void Update()
    {

        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = 3f;  // ※Canvasよりは手前に位置させること
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        // マウス操作
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(particlePrefab, mousePosition, Quaternion.identity); //実体化
        }
        if (Input.GetMouseButton(0))
        {
            var instance2 = Instantiate(DRAG_PARTICLE, mousePosition, Quaternion.identity); //実体化
        }
        /*遊び要素生成
        if (Input.GetMouseButtonDown(1))
        {
            GameObject instcard = Instantiate(rigidprefab, mousePosition, Quaternion.identity); //実体化
            instcard.AddComponent<Rigidbody>();
        }
        */
    }

    /*アンドロイド用
    if (Input.touchCount > 0)
    {
        Touch touch = Input.GetTouch(0);
        this._touch_position = touch.position;
        clickPosition = new Vector3(_touch_position.x, _touch_position.y, 3f);
        clickPosition = Camera.main.ScreenToWorldPoint(clickPosition); //座標をメインカメラ用に変更
        var instance = Instantiate(particlePrefab, clickPosition, Quaternion.identity); //実体化
    }
    */
}