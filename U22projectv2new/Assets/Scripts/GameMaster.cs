﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;
using UnityEngine.SceneManagement;
using TMPro;

public class GameMaster : MonoBehaviour
{
    int gameturncount = 1;
    int gamephase = 0;
    int phasecount = 0;
    int enemycardzoomid;
    int p1setzoomid;
    int p1setzoomnum;
    float cardspan = 0.5f;
    float cardmovetime;
    Vector3 p1position, p2position;
    Vector3[] p1setposition = new Vector3[3];
    Vector3[] p2setposition = new Vector3[3];
    Quaternion[] p1setQuaternion = new Quaternion[3];
    Quaternion[] p2setQuaternion = new Quaternion[3];
    Image player1guageimage,player2guageimage;
    bool enemycardsetmoveflg;
    bool battlezoombackflg;
    bool debuglogflg=true;
    bool[] p1setzoomflg = new bool[3];
    bool[] p2setzoomflg = new bool[3];
    bool battlezoommoveflag;
    public HTTPscript http;
    public bool turnendbtnflg;
    public bool decktypeisDEF = true;
    public bool deckchangeflg = true;
    public bool[,] defcard_table = new bool[8, 12];//防御ID、攻撃ID　防御IDから見た防げるID
    public bool[,] defcard_table2 = new bool[20, 20];//相互に対応しているID
    public bool[] setdefpos = new bool[20];
    public static int maxmana = 1;
    int[] cooldownlist = { 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 2, 2, 2, 3, 3, 3, 4 };
    int[] cardcooldown = new int[20];
    int[] cardcostint = new int[20];
    string[] cardcost;
    int[] carddamageint = new int[20];
    string[] carddamage;
    private EventSystem eventsys;

    public static PlayerClass[] player = new PlayerClass[] { null, new PlayerClass(), new PlayerClass() };
    //player[1]が自分、player[2]が相手　player[0]は未使用

    GameObject[] handcard = new GameObject[12];
    GameObject[] enemysetcardobj = new GameObject[3];
    Transform[] p1setTransf = new Transform[3], p2setTransf = new Transform[3];//セットカードのtransformキャッシュ用
    public GameObject[] player1setcardobj = new GameObject[3];
    public Image[] deficonobjimage = new Image[20];

    [SerializeField] GameObject DefIcon = default, Canvas = default, TurnLogo = default, TurnLogoText = default, TurnCount = default, PlayerStatsHud1P = default, PlayerStatsHud2P = default, Card = default, EnemyCard = default, BlockArea = default, bursteffect = default, ClickJacking = default, AttackEffect = default, Cardnew = default, CardnewEnemy = default;
    [SerializeField] Text PlayerStatsHud1Ptext, PlayerStatsHud2Ptext, DebugText;
    [SerializeField] Texture[] CardTexture;
    [SerializeField] ScrollRect DebugScrollRect;
    [SerializeField] ContentSizeFitter Csf;//デバッグログのサイズ取得用
    [SerializeField] ParticleSystem P1SieldParticle, P2SieldParticle;
    [SerializeField] ParticleSystem[] P1AttackParticle, P2AttackParticle;
    [SerializeField] TextMeshPro P1name,P2name;
    [SerializeField] Sprite[] TempSprite;
    AudioSource[] Audio;
    Animator TurnLogoAnime;
    TextReadSplitter txtsplit = new TextReadSplitter();//テキストを読み込んで分割するクラス

    //card.txt=0,4,1s1,7,2s2,8,2s3,10,3s4,13,4s5,15,4s6,17,5s7,18,5s8,20,6s9,21,6s10,23,7s11,25,8s12,0,1s13,0,2s14,0,2s15,0,3s16,0,4s17,0,4s18,0,5s19,0,6
    //taiou.txt=0,13,14,-1,-1,-1s1,17,19,-1,-1,-1s2,13,18,-1,-1,-1s3,12,15,-1,-1,-1s4,17,19,-1,-1,-1s5,12,18,-1,-1,-1s6,12,15,-1,-1,-1s7,14,19,-1,-1,-1s8,12,18,-1,-1,-1s9,16,18,-1,-1,-1s10,17,19,-1,-1,-1s11,12,16,-1,-1,-1s12,3,5,6,8,11s13,0,2,-1,-1,-1s14,0,7,-1,-1,-1s15,3,6,-1,-1,-1s16,9,11,-1,-1,-1s17,1,4,10,-1,-1s18,5,8,9,-1,-1s19,1,2,4,7,10

    //string testGETtest = "1,-1,-1,s,-1,98,100,s,-1,2,2,s,0,5,9,s,2,10,0,s,6,7,5,s,0,20,0";
    string testGETtest = "1,-1,-1,s,-1,98,100,s,-1,2,2,s,0,3,12,s,2,5,5,s,17,6,-1,s,2,0,5";
    string taiou = "0,13,14,-1,-1,-1,s,1,17,19,-1,-1,-1,s,2,13,18,-1,-1,-1,s,3,12,15,-1,-1,-1,s,4,17,19,-1,-1,-1,s,5,12,18,-1,-1,-1,s,6,12,15,-1,-1,-1,s,7,14,19,-1,-1,-1,s,8,12,18,-1,-1,-1,s,9,16,18,-1,-1,-1,s,10,17,19,-1,-1,-1,s,11,12,16,-1,-1,-1,s,12,3,5,6,8,11,s,13,0,2,-1,-1,-1,s,14,0,7,-1,-1,-1,s,15,3,6,-1,-1,-1,s,16,9,11,-1,-1,-1,s,17,1,4,10,-1,-1,s,18,5,8,9,-1,-1,s,19,1,2,4,7,10";

    string[] cardtext = {"パスワードの可能な組み合わせをすべて試し、不正にアクセスする攻撃",
                        "本来閲覧できないファイルを外部からすり抜けて閲覧する",
                        "サーバへ大量のデータや要求を送り付けサーバが処理しきれない状態にする",
                        "サイトに透明状態の悪影響を及ぼすサイトのリンクを配置する",
                        "直接データベースへ不正なコードを送り付けデータを改ざんしたり抜き取る",
                        "標的がよく訪問するサイトに悪影響を及ぼすプログラムを埋め込み、訪問するのを待つ(罠を仕掛ける)",
                        "悪意のあるサイトへ誘導し不正なプログラムをパソコンに実行させる",
                        "IPアドレスを偽装してサーバやパソコンにアクセスする",
                        "ウイルスを含んだファイルをユーザに許可を求めることなくダウンロードさせる",
                        "パソコン上のデータを暗号化し、元に戻すには身代金を支払うように要求するマルウェア(人質のようなもの)",
                        "パソコンやサーバのOSへ不正なコマンドを送りこみデータの改ざん削除を行う",
                        "マルウェアや感染したファイルを送り付ける",
                        "悪意のあるメールやサイトの注意喚起",
                        "特定のIPアドレスからの通信を拒否",
                        "2段階認証を行う",
                        "アクセスするサイトを制限する",
                        "セキュリティ対策ソフトによるファイルスキャン",
                        "データベースに害を及ぼす特殊文字(＆や＜、＞)を無害化する",
                        "OSのアップデートを行う",
                        "IPSでの通信監視、遮断"};
    string[] cardtitle = {"ブルートフォース\nアタック",
                        "ディレクトリ\nトラバーサル攻撃",
                        "DDos攻撃",
                        "クリック\nジャッキング",
                        "SQL\nインジェクション",
                        "水飲み場攻撃",
                        "クロスサイト\nスクリプティング",
                        "IPスプーフィング",
                        "ドライブバイ\nダウンロード",
                        "ランサムウェア",
                        "OSコマンド\nインジェクション",
                        "標的型メール",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""};

    string[] cardtitle2 = {"ブルートフォースアタック",
                        "ディレクトリトラバーサル攻撃",
                        "DDos攻撃",
                        "クリックジャッキング",
                        "SQLインジェクション",
                        "水飲み場攻撃",
                        "クロスサイトスクリプティング",
                        "IPスプーフィング",
                        "ドライブバイダウンロード",
                        "ランサムウェア",
                        "OSコマンドインジェクション",
                        "標的型メール",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""};

    GameObject[] MyCards = new GameObject[20];//最初にカードを20枚生成して格納しておく
    CardNew[] MyCardsScript = new CardNew[20];//カードのスクリプト指示用配列
    GameObject[] EnemyCards = new GameObject[20];//相手カードを20枚生成して格納しておく
    EnemyCardSc[] EnemyCardsScript = new EnemyCardSc[20];//カードのスクリプト指示用配列

    void Start()
    {
        Audio =  GetComponents<AudioSource>();//アタッチしてあるオーディオを一括取得
        if (GameObject.Find("HTTPobject") != null)//オンライン時
        {
            http = GameObject.Find("HTTPobject").GetComponent<HTTPscript>();//タイトルシーンから引き継いだオブジェクトのスクリプト
            SetPlayer1_ID_and_PhaseID(int.Parse(http.userNumber));//通信で取得したIDと行動順割り振り
            /*
            P1name.text = http.namestr[player[1].playerid];
            P2name.text = http.namestr[player[2].playerid];
            */
            ////////////////////カードコストとダメージを割り当て
            int[,] allray = new int[20, 3];
            for (int i = 0; i < 20; i++)
            {
                int[] temp = txtsplit.TextReadSplit_array(i, allray, http.cardtable);
                cardcostint[i] = temp[2];
                carddamageint[i] = temp[1];
            }
            //////////////対応表割り当て
            int[,] allray2 = new int[20, 6];
            for (int i = 12; i < 20; i++)//defcard_tableの部分
            {
                int[] temp = txtsplit.TextReadSplit_array(i, allray2, http.taioulist);
                for (int j = 1; j < 6; j++)
                {
                    if (temp[j] != -1)
                    {
                        defcard_table[i - 12, temp[j]] = true;
                    }
                }
            }
            for (int i = 0; i < 20; i++)//defcard_table2の部分
            {
                int[] temp = txtsplit.TextReadSplit_array(i, allray2, http.taioulist);
                for (int j = 1; j < 6; j++)
                {
                    if (temp[j] != -1)
                    {
                        defcard_table2[i, temp[j]] = true;
                    }
                }
            }
            int[] cooltest = {0,0,0,1,1,1,2,2,2,2,3,3,3,0,0,0,1,1,1,2 };
            cardcooldown = cooltest;

        }
        else startdebug();//オフライン環境用
        
        AddDebugText("roomID = " + http.roomID + "\nuserNumber=" + http.userNumber + "\nplayerID = " + http.userID);//デバッグテキストを表示
     
        //キャラクターシンボルの座標を取得
        p1position = GameObject.Find("Capsule1").GetComponent<Transform>().position;
        p2position = GameObject.Find("Capsule2").GetComponent<Transform>().position;
        ////ゲージ画像取得
        player1guageimage = PlayerStatsHud1P.GetComponent<RectTransform>().GetComponent<Image>();
        player2guageimage = PlayerStatsHud2P.GetComponent<RectTransform>().GetComponent<Image>();
        player1guageimage.fillAmount = 1.0f;
        player2guageimage.fillAmount = 1.0f;
        TurnLogoAnime = TurnLogo.GetComponent<Animator>();//ターンロゴ再生用コンポーネントを取得
        TurnLogoAnime.speed = 0;
        for (int i = 0; i < 20; i++)//防御場所アイコン表示用初期生成
        {
            GameObject DefIconObj = Instantiate(DefIcon);
            DefIconObj.transform.SetParent(Canvas.transform, false);
            DefIconObj.transform.SetSiblingIndex(1);
            RectTransform rect = DefIconObj.GetComponent<RectTransform>();
            deficonobjimage[i] = DefIconObj.GetComponent<Image>();
            rect.localScale = new Vector3(0.2149419f, 0.2585092f, 0.2515543f);
            if (i < 12) rect.localPosition = new Vector3(-7.2f + i * 25.6f, 188.7f, -6.6f);
            else
            {
                rect.localPosition = new Vector3(-7.2f + (i - 12) * 25.6f, 156.6f, -6.6f);
            }
        }
        Invoke("PlayFirstTrunLogo", 3);//初回のロゴ再生用のメソッドを呼ぶ。（,3）は3秒後
        /*
        //ブロック12個表示テスト
        for (int i = 0; i < 12; i++) {
            Instantiate(BlockArea, new Vector3(-1.61f + i*0.3f, 0.2f, -4.9f + i*0.22f), Quaternion.Euler(180, 0, 0));
        }
        */
        //対応表とカードテキストとカードタイトルからカードの対策テキストを作成
        string[] MeasuresText= new string[20];
        string tempstr="";
        for (int i = 0; i < 20; i++)
        {
            if (i < 12)MeasuresText[i]="　　　  対策手段\n";//攻撃カードだったら
            else MeasuresText[i] = "　　 防げる攻撃手段\n";//防御カードだったら
            for (int k = 0; k < 20; k++)
            {
                if (defcard_table2[i, k])//対応していたら
                {
                    if (i < 12) tempstr += "「"+cardtext[k] + "」\n";//攻撃カードだったら
                    else tempstr +="「" +cardtitle2[k] + "」\n";//防御カードだったら
                }
            }
            MeasuresText[i]+=tempstr;
            tempstr = "";
        }

        ////////初期カード20枚生成//////

        for (int i = 0; i < 20; i++)
        {
            MyCards[i] = Instantiate(Cardnew);
            if(i<12){
            MyCards[i].GetComponent<Renderer>().material.mainTexture = CardTexture[1];//攻撃カードに画像切替
             //Texture texture1 = (Texture)Resources.Load("Attack_Cards/atkcimg"+i);
             //Texture texture1 = (Texture)Resources.Load("Deffence_Cards/dcimg" + i);
            }
            MyCardsScript[i] = MyCards[i].GetComponent<CardNew>();
            MyCardsScript[i].SetStatus(this, i, cardcooldown[i], carddamageint[i], cardtitle[i], cardtext[i], MeasuresText[i]);//
            MyCards[i].name = "MyCard" + i;
        }

        ////////相手カード20枚生成//////

        for (int i = 0; i < 20; i++)
        {
            EnemyCards[i] = Instantiate(CardnewEnemy);
            if (i < 12)
            {
                EnemyCards[i].GetComponent<Renderer>().material.mainTexture = CardTexture[1];//攻撃カードに画像切替
                                                                                          //Texture texture1 = (Texture)Resources.Load("Attack_Cards/atkcimg"+i);
                                                                                          //Texture texture1 = (Texture)Resources.Load("Deffence_Cards/dcimg" + i);
            }
            EnemyCardsScript[i] = EnemyCards[i].GetComponent<EnemyCardSc>();
            EnemyCardsScript[i].SetStatus(this, i, cardcooldown[i], carddamageint[i], cardtitle[i], cardtext[i], MeasuresText[i]);//
            EnemyCards[i].name = "EnemyCard" + i;
        }

    }
    /// ここまでstart処理
    /// //////////////////////////////////////////////////////////////////////////////////////////
    /// 
    void PlayFirstTrunLogo()//初回のロゴ再生用
    {
        DrawDef();//初期手札ドロー
        TurnLogoAnime.speed = 1;
        TurnLogoChange(1);
    }

    void SetPlayer1_ID_and_PhaseID(int num)//ID割り振りメソッド
    {
        switch (num)
        {
            case 1:
                player[1].playerid = 1;
                player[2].playerid = 2;
                break;
            case 2:
                player[1].playerid = 2;
                player[2].playerid = 1;
                break;
            default:
                break;
        }
    }
    /*フィールドの色変え処理//不要？
    void Player2method()
    {
        Color newCol;
        ColorUtility.TryParseHtmlString("#D91E27", out newCol);
        GameObject.Find("Base").GetComponent<Renderer>().material.color = newCol;
        ColorUtility.TryParseHtmlString("#2B79C3", out newCol);
        GameObject.Find("Base2").GetComponent<Renderer>().material.color = newCol;
    }*/
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Keypad1))//ダメージ処理テスト
        {
            //DamageMethod(1, 5);
            AttackEffectMethod(1, 0);
        }
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            //DamageMethod(2, 5);
            AttackEffectMethod(1, 1);
        }
        if (Input.GetKeyDown(KeyCode.F1))//デバッグログの停止切替
        {
            if (debuglogflg) debuglogflg = false;
            else debuglogflg = true;
        }
        if (Input.GetKeyDown(KeyCode.F2))//デバッグ プレイヤー情報
        {
            http.response_str= "0,-1,-1s-1,-14,58s-1,3,3s0,1,2s0,7,0s14,13,0s-2,-6,-4";
            http.uploading = false;
            http.connecting = false;
        }
        if (Input.GetKeyDown(KeyCode.F3))//デバッグ 両プレイヤー行動完了用
        {
            http.response_str = "-1,1,1s2,1,1";
            http.uploading = false;
            http.connecting = false;
        }
        if (Input.GetKeyDown(KeyCode.F4))//デバッグ プレイヤー情報
        {
            http.response_str = "0,-1,-1s-1,-14,58s-1,3,3s7,2,0s0,0,0s13,19,14s-6,-13,-11";
            http.uploading = false;
            http.connecting = false;
        }
        if (Input.GetKeyDown(KeyCode.F5))//デバッグ プレイヤー情報
        {
            http.response_str = "0,-1,-1s-1,-14,58s-1,3,3s7,0,12s0,0,22s8,14,11s0,-11,0";
            http.uploading = false;
            http.connecting = false;
        }
        if (Input.GetKeyDown(KeyCode.F6))//デバッグ 後攻プレイヤーの先行行動待用
        {
            http.response_str = "0,-1,-1s-1,-14,58s-1,3,3s12,19,14s12,7,8s3,5,7s0,0,0";
            http.uploading = false;
            http.connecting = false;
        }
        if (Input.GetKeyDown(KeyCode.Z))//パーティクル光らせ
        {
            P1SieldParticle.Play();
            P2SieldParticle.Play();
        }
        /*
        if (battlezoommoveflag == true)//戦闘演出時のカード拡大移動処理
        {
            /*
            int p1zoomcount = 0, p2zoomcount = 0;
            for (int i = 0; i < 3; i++)
            {
                if (p1setzoomflg[i] == true) p1zoomcount++;
                if (p2setzoomflg[i] == true) p2zoomcount++;
            }
            */
            /*
            for (int i = 0; i < 3; i++)
            {
                if (p1setzoomflg[i] == true)
                {
                    if (player1setcardobj[i] != null)
                    {
                        player1setcardobj[i].transform.position = Vector3.MoveTowards(player1setcardobj[i].transform.position, new Vector3(0.178f + i * 0.1f, 2.775f, -5.384f), 0.2f);
                        player1setcardobj[i].transform.rotation = Quaternion.RotateTowards(player1setcardobj[i].transform.rotation, Quaternion.Euler(-27.473f, 6.668f, 7.013f), 8f);
                    }
                }
                if (p2setzoomflg[i] == true)
                {
                    if (enemysetcardobj[i] != null)
                    {
                    }
                }
                if (battlezoombackflg == true)//戦闘演出時のカードを元の位置に移動する処理
                {
                    if (p1setzoomflg[i] == false)
                    {
                        if (player1setcardobj[i] != null)
                        {
                            player1setcardobj[i].transform.position = Vector3.MoveTowards(player1setcardobj[i].transform.position, p1setposition[i], 0.4f);
                            player1setcardobj[i].transform.rotation = Quaternion.RotateTowards(player1setcardobj[i].transform.rotation, p1setQuaternion[i], 15f);
                        }
                    }
                    if (p2setzoomflg[i] == false)
                    {
                        if (enemysetcardobj[i] != null)
                        {
                            enemysetcardobj[i].transform.position = Vector3.MoveTowards(enemysetcardobj[i].transform.position, p2setposition[i], 0.4f);
                            enemysetcardobj[i].transform.rotation = Quaternion.RotateTowards(enemysetcardobj[i].transform.rotation, p2setQuaternion[i], 15f);
                        }
                    }
                }
            }
        }
        */
        /*
        if (battlezoombackflg == true)//戦闘演出時のカードを元の位置に移動する処理
        {
            for (int i = 0; i < 3; i++)
            {
                if (p1setzoomflg[i] == true)
                {
                    player1setcardobj[i].transform.position = Vector3.MoveTowards(player1setcardobj[i].transform.position, p1setposition[i],0.4f);
                    player1setcardobj[i].transform.rotation = Quaternion.RotateTowards(player1setcardobj[i].transform.rotation, p1setQuaternion[i], 15f);
                }
                if (p2setzoomflg[i] == false)
                {
                    enemysetcardobj[i].transform.position = Vector3.MoveTowards(enemysetcardobj[i].transform.position, p2setposition[i], 0.4f);
                    enemysetcardobj[i].transform.rotation = Quaternion.RotateTowards(enemysetcardobj[i].transform.rotation, p2setQuaternion[i], 15f);

                }
            }
        }*/


    }
    public void TurnChangeButton()//ターンエンドボタンを押した時
    {
        ClickJacking.SetActive(true);//クリック判定停止
        http.Usecardupload(player[1].setcardid);//使用手札をアップロード
        CardSelectParticleStop();//手札の選択パーティクルを止める
        StartCoroutine(GetEnemyStatus());//相手の行動待機
        TurnLogoChange(2);
        TurnLogoAnime.Play("Expand", 0, 0.0f);//ターンロゴを再生
    }

    void TurnLogoChange(int getphase)//ターンロゴの表示を書き換え
    {
        Color newCol;
        Audio[1].Play();//ターンチェンジ効果音
        if (getphase == 1)//セットフェイズ
        {
            TurnLogoText.GetComponent<Text>().text = ("SET PHASE");
            foreach (var target in GameObject.FindGameObjectsWithTag("ScrollText"))
            {
                target.GetComponent<Text>().text = ("SET PHASE   SET PHASE   SET PHASE");
            }
            ColorUtility.TryParseHtmlString("#5193FF", out newCol);
            TurnLogoText.GetComponent<Outline>().effectColor = newCol;
        }
        if (getphase == 2)//相手の行動待ち
        {
            TurnLogoText.GetComponent<Text>().text = ("Waiting for P2");
            foreach (var target in GameObject.FindGameObjectsWithTag("ScrollText"))
            {
                target.GetComponent<Text>().text = ("Waiting for P2 Waiting for P2");
            }
            ColorUtility.TryParseHtmlString("#5193FF", out newCol);
            TurnLogoText.GetComponent<Outline>().effectColor = newCol;
        }
        if (getphase == 3)//バトルフェイズ
        {
            TurnLogoText.GetComponent<Text>().text = ("BATTLE PHASE");
            foreach (var target in GameObject.FindGameObjectsWithTag("ScrollText"))
            {
                target.GetComponent<Text>().text = ("BATTLE PHASE   BATTLE PHASE   BATTLE PHASE");
            }
            ColorUtility.TryParseHtmlString("#FF3B00", out newCol);
            TurnLogoText.GetComponent<Outline>().effectColor = newCol;
        }
    }
    /// <summary>
    /// /////////////////////////////ドロー処理///////////////////

    public void CardPosResetOrder()//手札の位置をリセットする指示を送る
    {
        for (int i = 0; i < 20; i++)
        {
            //セットカードに該当IDがあったらリセットしない
            if (player[1].setcardid[0] != i && player[1].setcardid[1] != i && player[1].setcardid[2] != i)
            {
                MyCardsScript[i].CardPosReset();
            }
        }
    }

    public void DrawDef()
    {
        deckchangeflg = false;//全部引き終わるまでカードチェンジできないように
        StartCoroutine(DrawDefCorl());
    }
    IEnumerator DrawDefCorl()
    {
        for (int i = 12; i < 20; i++)
        {
            //セットカードに該当IDがあったら引かない
            if (player[1].setcardid[0] != i && player[1].setcardid[1] != i && player[1].setcardid[2] != i)
            {
                MyCards[i].SetActive(true);
                MyCardsScript[i].Draw(i-12);
                yield return new WaitForSeconds(0.1f);
            }
        }
        if (ClickJacking.active == false)
        {
            for (int i = 12; i < 20; i++)//操作可能状態の時クールダウンではないカードパーティクルを光らせる
            {
                if (player[1].setcardid[0] != i && player[1].setcardid[1] != i && player[1].setcardid[2] != i)
                {
                    MyCardsScript[i].SelectParticlePlay();
                }
            }
        }
        deckchangeflg = true;
    }

    public void DrawATK()
    {
        deckchangeflg = false;
        StartCoroutine(DrawATKCorl());
    }
    IEnumerator DrawATKCorl()
    {

        for (int i = 0; i < 12; i++)
        {
            //セットカードに該当IDがあったら引かない
            if (player[1].setcardid[0] != i && player[1].setcardid[1] != i && player[1].setcardid[2] != i)
            {
                MyCards[i].SetActive(true);
                MyCardsScript[i].Draw(i);
                yield return new WaitForSeconds(0.1f);
            }
        }
        if (ClickJacking.active == false)
        {
            for (int i = 0; i < 12; i++)//操作可能状態の時クールダウンではないカードパーティクルを光らせる
            {
                if (player[1].setcardid[0] != i && player[1].setcardid[1] != i && player[1].setcardid[2] != i)
                {
                    MyCardsScript[i].SelectParticlePlay();
                }
            }
        }
        deckchangeflg = true;
    }

    IEnumerator GetEnemyStatus()//相手の行動を待つ通信処理
    {
        ClickJacking.SetActive(true);//イベントシステムを止めてクリック判定無効化
        while (http.uploading==true)yield return new WaitForSeconds(1f);//アップロードが完了するまで毎秒待機
        http.Gettext("room.txt");//部屋状況表を取得
        yield return new WaitForSeconds(1.5f);//1秒待機
        while (http.response_str == "" || http.response_str[7] != '2')//両プレイヤーの行動が完了していない間ループ
        {
            AddDebugText("room状況取得中 "+ http.response_str);
            http.Gettext("room.txt");//部屋状況表を再び取得
            yield return new WaitForSeconds(1.5f);//1秒待機
        }
        AddDebugText("room.txt両プレイヤー行動確認＝" + http.response_str);
        http.Gettext((player[1].playerid+".txt"));//自分の処理結果を取得
        yield return new WaitForSeconds(1f);//1秒待機
        while (http.response_str == "")//情報が取得できるまでループ
        {
            http.Gettext(player[1].playerid + ".txt");//自分の処理結果を再び取得
            yield return new WaitForSeconds(1f);//1秒待機
            AddDebugText(player[1].playerid+".txt取得中 " + http.response_str);
        }
        AddDebugText("取得完了\n" + http.response_str);//デバッグテキストに取得したテキストを表示

        TurnLogoChange(3);
        TurnLogoAnime.Play("Expand", 0, 0.0f);//ターンロゴを再生
        /////バトルフェイズへ////
        /*
        }

        

        IEnumerator BattlePhase()//演出処理//
        {
        */

        int[,] allray = new int[7, 3];
        int[] enemyusecard = new int[3];
        enemyusecard = txtsplit.TextReadSplit_array(5, allray, http.response_str);//相手プレイヤー情報の使用カード行を取得
        AddDebugText("相手手札" + enemyusecard[0] + " " + enemyusecard[1] + " " + enemyusecard[2]);
        for (int i = 0; i < 3; i++)//敵のセットカードを割当
        {
            player[2].setcardid[i] = enemyusecard[i];//取得したテキストから相手カード割り当て
            if (enemyusecard[i] != -1)//カードを選択していたら -1は未使用
            {
                /*旧処理
                enemysetcardobj[i] = Instantiate(EnemyCard, p2position, Quaternion.Euler(180, -48, 0));
                enemysetcardobj[i].name = "EnemyCard" + i;
                if (enemyusecard[i] < 12) enemysetcardobj[i].GetComponent<Renderer>().material.mainTexture = CardTexture[1];//IDに応じて攻撃カードにテクスチャ貼り替え
                player[2].cardsetcount++;
                AddDebugText(enemyusecard[i].ToString());
                //enemysetcardobj[i].GetComponent<EnemyCardSc>().Setcardstatus(enemyusecard[i], cardcostint[enemyusecard[i]], carddamageint[enemyusecard[i]]);
                */
                EnemyCardsScript[enemyusecard[i]].SetMove(player[2].cardsetcount,p2position);
                player[2].cardsetcount++;
                Audio[3].Play();
            }
        }
        yield return new WaitForSeconds(1.3f);
        int[] success_damage = new int[3];//与えたダメージ
        int[] get_damage = new int[3];//受けたダメージ
        success_damage = txtsplit.TextReadSplit_array(4, allray, http.response_str);
        get_damage = txtsplit.TextReadSplit_array(6, allray, http.response_str);
        CardOpen();//カードオープン
        Audio[3].Play();
        yield return new WaitForSeconds(1.3f);
        for (int i = 0; i < 3; i++)//相手の防御カードを処理
        {
            int tempP1i = player[1].setcardid[i];
            int tempP2i = player[2].setcardid[i];
            int multicount = 0;
            if (tempP2i > 11)//相手が防御カードを出していたら
            {
                EnemyCardsScript[tempP2i].SetZoom(0);//相手の出したカードをズーム
                for (int j = 0; j < 3; j++)
                {
                    if (player[1].setcardid[j]!=-1&&defcard_table2[tempP2i, player[1].setcardid[j]] == true)//相手の防御カードに対応するカードを自分が出したか確認
                    {
                        MyCardsScript[player[1].setcardid[j]].SetZoom(multicount);//対応している自分のセットカードをズームする
                        multicount++;
                    }
                }
                yield return new WaitForSeconds(1.2f);
                //カード拡大完了
                if (get_damage[i]!=0)//反射されていたら通る
                {
                    P2SieldParticle.Play();
                    AttackEffectMethod(1, 0);
                    yield return new WaitForSeconds(0.2f);
                    AttackEffectMethod(2, 0);
                    Audio[6].Play();//反射効果音
                    yield return new WaitForSeconds(0.3f);
                    DamageMethod(1, (get_damage[i] * -1));//text6行目のi列目のダメージを受ける
                    yield return new WaitForSeconds(0.7f);
                }
                //ズームから戻す
                EnemyCardsScript[tempP2i].SetZoomReverse();
                for (int j = 0; j < 3; j++)
                {
                    if (player[1].setcardid[j] != -1)
                    {
                        MyCardsScript[player[1].setcardid[j]].SetZoomReverse();
                    }
                }
                yield return new WaitForSeconds(0.7f);
            }
        }
        ////自分の防御カードを処理
        for (int i = 0; i < 3; i++)
        {
            int tempP1i = player[1].setcardid[i];
            int tempP2i = player[2].setcardid[i];
            int multicount = 0;
            if (tempP1i > 11)//自分が防御カードを出していたら
            {
                MyCardsScript[tempP1i].SetZoom(0);//自分の出したカードをズーム
                for (int j = 0; j < 3; j++)
                {
                    if (player[2].setcardid[j] != -1 && defcard_table2[tempP1i, player[2].setcardid[j]] == true)//自分の防御カードに対応するカードを相手が出したか確認
                    {
                        EnemyCardsScript[player[2].setcardid[j]].SetZoom(multicount);//自分防御に対応している相手のセットカードをズームする
                        multicount++;
                    }
                }
                yield return new WaitForSeconds(1.2f);
                //カード拡大完了
                if (success_damage[i] != 0)//反射成功していたら通る
                {
                    P1SieldParticle.Play();
                    AttackEffectMethod(2, 0);
                    yield return new WaitForSeconds(0.2f);
                    Audio[6].Play();//反射効果音
                    AttackEffectMethod(1, 0);
                    yield return new WaitForSeconds(0.3f);
                    DamageMethod(2, (success_damage[i]));//text4行目のi列目のダメージを与える
                    yield return new WaitForSeconds(0.7f);
                }
                //ズームから戻す
                MyCardsScript[tempP1i].SetZoomReverse();
                for (int j = 0; j < 3; j++)
                {
                    if (player[2].setcardid[j] != -1)
                    {
                        EnemyCardsScript[player[2].setcardid[j]].SetZoomReverse();
                    }
                }
                yield return new WaitForSeconds(0.7f);
            }
        }
        for (int i = 0; i < 3; i++)//残りの攻撃カードを処理
        {
            int tempP1i = player[1].setcardid[i];
            int tempP2i = player[2].setcardid[i];
            if (tempP1i!=-1&&tempP1i<12&&success_damage[i]!=0)
            {
                MyCardsScript[tempP1i].SetZoom(0);//自分の出したカードをズーム
                yield return new WaitForSeconds(1.2f);
                AttackEffectMethod(1, 0);
                yield return new WaitForSeconds(0.3f);
                DamageMethod(2, (success_damage[i]));//text4行目のi列目のダメージを与える
                yield return new WaitForSeconds(0.7f);
                MyCardsScript[tempP1i].SetZoomReverse();//ズームから戻す
                yield return new WaitForSeconds(0.7f);
            }
            if (tempP2i != -1 && tempP2i < 12 && get_damage[i] != 0)
            {
                EnemyCardsScript[tempP2i].SetZoom(0);//相手の出したカードをズーム
                yield return new WaitForSeconds(1.2f);
                AttackEffectMethod(2, 0);
                yield return new WaitForSeconds(0.3f);
                DamageMethod(1, (get_damage[i] * -1));//text6行目のi列目のダメージを受ける
                yield return new WaitForSeconds(0.7f);
                EnemyCardsScript[tempP2i].SetZoomReverse();//ズームから戻す
                yield return new WaitForSeconds(0.7f);
            }
        }
        /*////////////////////////旧処理
        bool[] p2setusedflg = new bool[3];
        bool[] damaged = new bool[3];
        for (int k = 0; k < 3; k++)//セットカードに対応したカードがあるか調べる
        {
            int P1multicount = 0;
            int P2multicount = 0;
            if (player[1].setcardid[k] != -1 && damaged[k] == false)//何かしらセットしているか
            {
                for (int j = 0; j < 3; j++)//相手のカードの探索用
                {
                    if (player[2].setcardid[j] != -1 && defcard_table2[player[1].setcardid[k], player[2].setcardid[j]] == true)//自分のセットカードに対応するカードを相手が出したか確認
                    {
                        EnemyCardsScript[player[2].setcardid[j]].SetZoom(P2multicount);//対応してるカードをズーム
                        P2multicount++;
                        p2setusedflg[j] = true;
                    }
                }
                if (player[1].setcardid[k] > 11)// 防御カードを出している場合
                {
                    MyCardsScript[player[1].setcardid[k]].BattleZoom(P1multicount);//自分のセットカードのk番目をズームする
                    yield return new WaitForSeconds(1f);
                    if (success_damage[k] != 0)//与えたダメージ
                    {
                        P1SieldParticle.Play();
                        AttackEffectMethod(2, 0);
                        AttackEffectMethod(1, 0);
                        yield return new WaitForSeconds(0.3f);
                        DamageMethod(2, success_damage[k]);//与えたダメージ
                        yield return new WaitForSeconds(1f);
                    }
                }
                else//攻撃カードをセットしている場合
                {
                    if (success_damage[k] != 0)//防がれなかった場合
                    {
                        MyCardsScript[player[1].setcardid[k]].BattleZoom(P1multicount);//自分のセットカードのk番目をズームする
                        yield return new WaitForSeconds(1f);
                        AttackEffectMethod(1, 0);
                        yield return new WaitForSeconds(0.3f);
                        DamageMethod(2, success_damage[k]);//与えたダメージ
                        yield return new WaitForSeconds(1f);
                    }
                    else//反射されて受けたダメージ
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (p2setusedflg[j])
                            {
                                for (int mm = 0; mm < 3; mm++)
                                {
                                    if (defcard_table2[player[1].setcardid[mm], player[2].setcardid[j]] == true)//自分のセットカードに対応するカードを相手が出したか確認
                                    {
                                        MyCardsScript[player[1].setcardid[mm]].BattleZoom(P1multicount);//対応してるカードをズーム
                                        P1multicount++;
                                    }

                                }
                                yield return new WaitForSeconds(1f);
                                P2SieldParticle.Play();
                                AttackEffectMethod(1, 0);
                                AttackEffectMethod(2, 0);
                                yield return new WaitForSeconds(0.3f);
                                DamageMethod(1, (get_damage[k] * -1));//相手セットカードのj番目の6行目のダメージを受ける
                                yield return new WaitForSeconds(0.7f);
                                damaged[j] = true;
                                p2setusedflg[j] = true;
                                break;
                            }
                        }

                    }
                }
                MyCardsScript[player[1].setcardid[k]].SetZoomReverse();//ズームから戻す
                for (int j = 0; j < 3; j++)//ズームから戻す
                {
                    if (player[1].setcardid[j] != -1)
                    {
                        MyCardsScript[player[1].setcardid[j]].SetZoomReverse();
                    }
                    if (player[2].setcardid[j] != -1)
                    {
                        EnemyCardsScript[player[2].setcardid[j]].SetZoomReverse();
                    }
                }
                yield return new WaitForSeconds(0.7f);
            }
        }
        for (int m = 0; m < 3; m++)//相手カードの処理していないカードを消化
         {
            if (player[2].setcardid[m] != -1 && p2setusedflg[m] == false)//何かしらセットしていて↑で使用されていないか
            {
                EnemyCardsScript[player[2].setcardid[m]].setzoomflag = true;//ズーム処理フラグ始動
                yield return new WaitForSeconds(0.7f);
                if (get_damage[m] != 0)
                {
                    AttackEffectMethod(2,0);
                    yield return new WaitForSeconds(0.3f);
                    DamageMethod(1, (get_damage[m] * -1));//相手セットカードのm番目の6行目のダメージを受ける
                    yield return new WaitForSeconds(0.7f);
                }
                EnemyCardsScript[player[2].setcardid[m]].SetZoomReverse();//元の位置に戻す処理
                yield return new WaitForSeconds(0.7f);
            }
        }
        *///////////ここまで旧処理

        //使用した相手カードをクールダウン表示位置に移動させる
        for (int i = 0; i < 3; i++)
        {
            if (player[2].setcardid[i] != -1)
            {
                EnemyCardsScript[player[2].setcardid[i]].SetCoolTime(cooldownlist[player[2].setcardid[i]]+1);//クールダウン割当
                EnemyCardsScript[player[2].setcardid[i]].CoolTimePosMove();
            }
        }
        yield return new WaitForSeconds(0.6f);
        if (GameEndCheck())//体力を調べて終了動画を流すメソッド
        {
            SceneManager.LoadScene("GameEnd");
        }
        //ゲーム継続
        else
        {
            gameturncount++;
        TurnCount.GetComponent<Text>().text = gameturncount.ToString();//ターン数書き換え
        phasecount = 0;
        ///////////ターン終了初期化処理////////////////
        SetCardCount(1, 0);//セットカード枚数を初期化
        SetCardCount(2, 0);
        for (int i = 0; i < 3; i++)//セットカードを初期化
        {
            if (player[1].setcardid[i]!=-1)
            {
                MyCardsScript[player[1].setcardid[i]].seted = false;
                MyCardsScript[player[1].setcardid[i]].SetCooldown(cooldownlist[player[1].setcardid[i]]+1);//クールダウン割当
                player[1].setcardid[i] = -1;
            }
            player[2].setcardid[i] = -1;
        }
        for (int i = 0; i < 20; i++)//防御位置アイコンを透明化
        {
            deficonobjimage[i].color = new Color(255, 255, 255, 0);
            setdefpos[i] = false;//防御位置を初期化
        }

        for (int i = 0; i < 20; i++)//クールタイムを割り当て
        {
            MyCardsScript[i].CardPosReset();
            MyCardsScript[i].DecreaseCooldown();
            EnemyCardsScript[i].DecreaseCooldown();
            EnemyCardsScript[i].CardPosReset();
        }
        if (decktypeisDEF) DrawDef();//最後に選択していたデッキ種類で再ドロー
        else DrawATK();
        /*
        ///////ターン毎の攻守切替
        if (player[1].phaseid == 1)
        {
            player[1].phaseid = 2;
            player[2].phaseid = 1;
            TurnLogoChange(2);
            StartCoroutine(GetEnemyStatus());//相手の行動待機
        }
        else
        {
            player[1].phaseid = 1;
            player[2].phaseid = 2;
            TurnLogoChange(1);
            ClickJacking.SetActive(false); ;//クリック判定復帰
        }
        */
        
        ClickJacking.SetActive(false); ;//クリック判定復帰
        TurnLogoChange(1);//セットフェイズロゴ
        TurnLogoAnime.Play("Expand", 0, 0.0f);//ターンロゴを再生
        }

    }

    void CardOpen()//両プレイヤーのカードオープン処理
    {
        for (int i = 0; i < 3; i++)
        {
            if (player[1].setcardid[i]!=-1)
            {
                MyCardsScript[player[1].setcardid[i]].cardopenflg = true;
            }
            if (player[2].setcardid[i]!=-1)
            {
                EnemyCardsScript[player[2].setcardid[i]].cardopenflag = true;
            }
        }
    }

    void AddDebugText(string str)//デバッグエリアに文字を追加
    {
        if (debuglogflg)
        {
            DebugText.text += str+"\n";
            Csf.SetLayoutVertical();//スクロールバーの位置を最下段に
            DebugScrollRect.verticalNormalizedPosition = 0;//スクロールバーの位置を最下段に
        }
        Debug.Log(str);
    }
    public void AttackEffectMethod(int getplayerid, int effectid)
    {
        Audio[5].Play();
        if(getplayerid==1) P1AttackParticle[effectid].Play();
        else P2AttackParticle[effectid].Play();
    }
    //ダメージ処理
    public void DamageMethod(int getplayerid, int damage)
    {
        Audio[2].Play();
        if (getplayerid==1)//自プレイヤーダメージ処理
        {
            player[1].hp -= damage;
            if (player[1].hp < 0) player[1].hp = 0;
            player1guageimage.fillAmount -= damage / 100f;
            PlayerStatsHud1Ptext.text = (player[1].hp).ToString() + "%";//ゲージのテキスト
            Instantiate(bursteffect, p1position, Quaternion.identity);//爆発エフェクト生成
        }
        else
        {
            player[2].hp -= damage;
            if (player[2].hp < 0) player[2].hp = 0;
            player2guageimage.fillAmount -= damage / 100f;
            PlayerStatsHud2Ptext.text = (player[2].hp).ToString() + "%"; ;//ゲージのテキスト
            Instantiate(bursteffect, p2position, Quaternion.identity);
        }
        /*バックアップ
        if (player[1].playerid == getplayerid)
        {
            player1guageimage.fillAmount -= damage / 100f;
            PlayerStatsHud1Ptext.text = (int)(player1guageimage.fillAmount * 100f) + "%";//ゲージのテキスト
            Instantiate(bursteffect, p1position, Quaternion.identity);//爆発エフェクト生成
        }
        else
        {
            player2guageimage.fillAmount -= damage / 100f;
            PlayerStatsHud2Ptext.text = (int)(player2guageimage.fillAmount * 100f) + "%";//ゲージのテキスト
            Instantiate(bursteffect, p2position, Quaternion.identity);

        }
        */
    }

    bool GameEndCheck()//終了演出
    {
        if (player[1].hp <= 0)
        {
            GameEndSc.endmovie = 0;
            return true;
        }
        if (player[2].hp <= 0)
        {
            GameEndSc.endmovie = 1;
            return true;
        }
        if (player[1].hp <=0 && player[2].hp <= 0)
        {
            GameEndSc.endmovie = 2;
            return true;
        }
        return false;
    }

    public void CardSelectParticleCheck()
    {
        for (int i = 0; i < 20; i++)
        {
            MyCardsScript[i].SelectParticlePlay();
        }
    }
    public void CardSelectParticleStop()
    {
        for (int i = 0; i < 20; i++)
        {
            MyCardsScript[i].SelectParticleStop();
        }
    }

    /////// ゲッター///////

    public int GetSetCardCount(int getid)
    {
        return player[getid].cardsetcount;
    }
    /////// セッター///////
    public void SetCardCount(int getid, int setcard)
    {
        player[getid].cardsetcount = setcard;
    }

    public void SetCardid(int getid, int cid)
    {
        player[1].setcardid[getid] = cid;
    }
    ///////////////////////////////////デバッグ時////////////////////////////////
    ///
    void startdebug()//オフライン環境の初期化処理
    {
        GameObject insthttp = (GameObject)Resources.Load("Prefabs/HTTPobject");
        GameObject offlinehttp = Instantiate(insthttp);
        http = offlinehttp.GetComponent<HTTPscript>();
        SetPlayer1_ID_and_PhaseID(1);
        string card_txt = "0,4,1s1,7,2s2,8,2s3,10,3s4,13,4s5,15,4s6,17,5s7,18,5s8,20,6s9,21,6s10,23,7s11,25,8s12,0,1s13,0,2s14,0,2s15,0,3s16,0,4s17,0,4s18,0,5s19,0,6";
        string taiou_txt = "0,13,14,-1,-1,-1s1,17,19,-1,-1,-1s2,13,18,-1,-1,-1s3,12,15,-1,-1,-1s4,17,19,-1,-1,-1s5,12,18,-1,-1,-1s6,12,15,-1,-1,-1s7,14,19,-1,-1,-1s8,12,18,-1,-1,-1s9,16,18,-1,-1,-1s10,17,19,-1,-1,-1s11,12,16,-1,-1,-1s12,3,5,6,8,11s13,0,2,-1,-1,-1s14,0,7,-1,-1,-1s15,3,6,-1,-1,-1s16,9,11,-1,-1,-1s17,1,4,10,-1,-1s18,5,8,9,-1,-1s19,1,2,4,7,10";

        ////////////////////カードコストとダメージを割り当て
        int[,] allray = new int[20, 3];
        for (int i = 0; i < 20; i++)
        {
            int[] temp = txtsplit.TextReadSplit_array(i, allray, card_txt);
            cardcostint[i] = temp[2];
            carddamageint[i] = temp[1];
        }
        //////////////対応表割り当て
        int[,] allray2 = new int[20, 6];
        for (int i = 12; i < 20; i++)//defcard_tableの部分
        {
            int[] temp = txtsplit.TextReadSplit_array(i, allray2, taiou_txt);
            for (int j = 1; j < 6; j++)
            {
                if (temp[j] != -1)
                {
                    defcard_table[i - 12, temp[j]] = true;
                }
            }
        }
        for (int i = 0; i < 20; i++)//defcard_table2の部分
        {
            int[] temp = txtsplit.TextReadSplit_array(i, allray2, taiou_txt);
            for (int j = 1; j < 6; j++)
            {
                if (temp[j] != -1)
                {
                    defcard_table2[i, temp[j]] = true;
                }
            }
        }
        /*
        int[] ccost = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 1, 2, 3, 4, 5, 6, 7, 8 };
        cardcostint = ccost;
        int[] cdmg = { 4, 7, 8, 10, 13, 15, 17, 18, 20, 21, 23, 25, 1, 2, 3, 4, 5, 6, 7, 8 };
        carddamageint = cdmg;
        */
        //int[] cool = { 0,0,0,1,1,1,2,2,2,2,3,3,3,0,0,0,1,1,1,2 };
        int[] cool = new int[20];
        cardcooldown = cool;

    }
}
